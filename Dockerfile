FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

# Install build-essential for native add-ons
RUN apt-get update && apt-get install -y build-essential

# Install git
RUN apt-get install -y git

RUN mkdir -p /app/code /app/data

# Clone the repository without full commit history
RUN git clone --depth 1 --branch main https://github.com/mckaywrigley/chatbot-ui.git /app/data
RUN rm -f Dockerfile

WORKDIR /app/data
RUN npm ci && npm run build

# Expose the port used by the app
EXPOSE 3000

# Start the app
CMD ["npm", "start", "--log-file", "/app/data/"]

