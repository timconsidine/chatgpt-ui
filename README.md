# chatgpt-ui

## This is a package of the ChatGPT-UI app.
See https://github.com/mckaywrigley/chatbot-ui

## this package uses a CHEAT
The standard start command `npm start` will attempt to create log files under `/root/_npm`

This breaks the app when it is installed to the conventional location of `/app/code` because it is a read-only file tree.

Therefore THIS package installs to a read-write location of `/app/data`

Installing to `/app/code` and using a modified log file location `npm start --log-file /app/data` does not seem to work.

Hence the cheat. 

Maybe you know how to resolve this.

In the interim, it runs from `/app/data` - sorry.

## Install
1. download the files to a local working directory
2. use the included utility script `cld.sh` to build the Docker image and install to your Cloudron installation
  - make sure you have Docker installed
  - make sure you are logged in to your docker repository (Docker Hub or your self-hosted one)
  - make sure you have installed Cloudron CLI tools
  - make sure you are logged in to your Cloudron instance
  - then enter `./cld.sh <your-docker-repo>/chatbot-ui-cloudron:<date><suffix>
  - then enter the `Location` for your app, e.g. chatbotui.<your-cloudron-domain>
3. Utility script is option : do it manually if you prefer.
4. Name your docker image however you want, with your preferred `tag` for the image.  I use `<date><letter suffix>` but you follow your own preferences.

## Running
1. Visit https://chatbotui.<your-cloudron-domain>
2. Enter the OpenAI API key which you have obtained from OpenAI
  - If you don't have an OpenAI API key, you can get one at https://platform.openai.com/account/api-keys.

## Notes
1.  This version of Chatbot-UI has introduced `Plugins` ... but I haven't used them so cannot offer guidance at this stage.
